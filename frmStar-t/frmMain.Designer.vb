﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuUsuario = New System.Windows.Forms.MenuStrip()
        Me.itemUsuario = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetallesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MiHistorialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialDeAdministradoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestiónCuentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministradoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CRUDDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUsuario.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuUsuario
        '
        Me.mnuUsuario.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.itemUsuario, Me.HistorialToolStripMenuItem, Me.GestiónCuentasToolStripMenuItem, Me.CRUDDBToolStripMenuItem})
        Me.mnuUsuario.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.mnuUsuario.Location = New System.Drawing.Point(0, 0)
        Me.mnuUsuario.Name = "mnuUsuario"
        Me.mnuUsuario.Size = New System.Drawing.Size(1079, 24)
        Me.mnuUsuario.TabIndex = 2
        '
        'itemUsuario
        '
        Me.itemUsuario.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetallesToolStripMenuItem, Me.CerrarSesiónToolStripMenuItem})
        Me.itemUsuario.Name = "itemUsuario"
        Me.itemUsuario.Size = New System.Drawing.Size(72, 20)
        Me.itemUsuario.Text = "&Mi cuenta"
        '
        'DetallesToolStripMenuItem
        '
        Me.DetallesToolStripMenuItem.Name = "DetallesToolStripMenuItem"
        Me.DetallesToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.DetallesToolStripMenuItem.Text = "&Detalles"
        '
        'CerrarSesiónToolStripMenuItem
        '
        Me.CerrarSesiónToolStripMenuItem.Name = "CerrarSesiónToolStripMenuItem"
        Me.CerrarSesiónToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.CerrarSesiónToolStripMenuItem.Text = "&Cerrar sesión"
        '
        'HistorialToolStripMenuItem
        '
        Me.HistorialToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MiHistorialToolStripMenuItem, Me.HistorialDeAdministradoresToolStripMenuItem})
        Me.HistorialToolStripMenuItem.Name = "HistorialToolStripMenuItem"
        Me.HistorialToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.HistorialToolStripMenuItem.Text = "&Historial"
        '
        'MiHistorialToolStripMenuItem
        '
        Me.MiHistorialToolStripMenuItem.Name = "MiHistorialToolStripMenuItem"
        Me.MiHistorialToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.MiHistorialToolStripMenuItem.Text = "&Mi historial"
        '
        'HistorialDeAdministradoresToolStripMenuItem
        '
        Me.HistorialDeAdministradoresToolStripMenuItem.Name = "HistorialDeAdministradoresToolStripMenuItem"
        Me.HistorialDeAdministradoresToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.HistorialDeAdministradoresToolStripMenuItem.Text = "&Historial de administradores"
        '
        'GestiónCuentasToolStripMenuItem
        '
        Me.GestiónCuentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosToolStripMenuItem, Me.AdministradoresToolStripMenuItem})
        Me.GestiónCuentasToolStripMenuItem.Name = "GestiónCuentasToolStripMenuItem"
        Me.GestiónCuentasToolStripMenuItem.Size = New System.Drawing.Size(103, 20)
        Me.GestiónCuentasToolStripMenuItem.Text = "Gestión cuentas"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.UsuariosToolStripMenuItem.Text = "Añadir usuarios"
        '
        'AdministradoresToolStripMenuItem
        '
        Me.AdministradoresToolStripMenuItem.Name = "AdministradoresToolStripMenuItem"
        Me.AdministradoresToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AdministradoresToolStripMenuItem.Text = "Administradores"
        '
        'CRUDDBToolStripMenuItem
        '
        Me.CRUDDBToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TipoServicioToolStripMenuItem})
        Me.CRUDDBToolStripMenuItem.Name = "CRUDDBToolStripMenuItem"
        Me.CRUDDBToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.CRUDDBToolStripMenuItem.Text = "CRUD DB"
        '
        'TipoServicioToolStripMenuItem
        '
        Me.TipoServicioToolStripMenuItem.Name = "TipoServicioToolStripMenuItem"
        Me.TipoServicioToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.TipoServicioToolStripMenuItem.Text = "Tipo Servicio"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1079, 563)
        Me.Controls.Add(Me.mnuUsuario)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuUsuario
        Me.Name = "frmMain"
        Me.Text = "Star-t"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuUsuario.ResumeLayout(False)
        Me.mnuUsuario.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mnuUsuario As MenuStrip
    Friend WithEvents itemUsuario As ToolStripMenuItem
    Friend WithEvents DetallesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CerrarSesiónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HistorialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MiHistorialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HistorialDeAdministradoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestiónCuentasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AdministradoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CRUDDBToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TipoServicioToolStripMenuItem As ToolStripMenuItem
End Class
