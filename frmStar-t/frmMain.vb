﻿Public Class frmMain

    Dim listHouses As New ListView

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        frmViewAdmins.MdiParent = Me

        'frmViewHouses.WindowState = FormWindowState.Maximized
        'frmViewHouses.Width = Me.ClientSize.Width * 0.9
        'frmViewHouses.Height = Me.ClientSize.Height * 0.8
        StartPosition = FormStartPosition.CenterParent

        frmViewEnchufess.MdiParent = Me
        frmViewEnchufess.Show()
        'frmViewHouses2.MdiParent = Me
        'frmViewHouses2.Show()
        'frmViewHouses2.WindowState = FormWindowState.Maximized

    End Sub

    Private Sub btnMiCuenta_Click(sender As Object, e As EventArgs)
        Dim formularito As New frmMiCuenta()
        formularito.MdiParent = Me

        formularito.Show()

    End Sub

    Private Sub DetallesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DetallesToolStripMenuItem.Click
        frmMiCuenta.MdiParent = Me

        frmMiCuenta.Show()
    End Sub

    Private Sub CerrarSesiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarSesiónToolStripMenuItem.Click
        frmLogin.Show()
        Me.Hide()
    End Sub


    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click
        frmNewUser.Show()
    End Sub

    Private Sub AdministradoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdministradoresToolStripMenuItem.Click
        frmViewAdmins.Show()
    End Sub

    Private Sub TipoServicioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TipoServicioToolStripMenuItem.Click
        frmTiposServicio.MdiParent = Me

        frmTiposServicio.Show()
    End Sub

End Class
