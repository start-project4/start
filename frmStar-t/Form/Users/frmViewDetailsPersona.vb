﻿Public Class frmViewDetailsPersona
    Private Sub viewDetailsHouse_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addEnchufe(1, "Lamparita mesa de luz", 2, "Apagado")
        addEnchufe(2, "Lavarropa", 202, "Prendido")
        addEnchufe(3, "Microondas", 402, "Error - Sin conexión")
        addEnchufe(4, "Lamparita mesa de luz", 482, "Error - Sin corriente")
    End Sub

    Private Sub addEnchufe(id As Integer, name As String, consumo As Double, state As String)
        Dim item As New ListViewItem(id)

        item.SubItems.Add(name)
        item.SubItems.Add(consumo)
        item.SubItems.Add(state)

        lvwEnchufes.Items.Add(item)
    End Sub

    Private Sub lvwEnchufes_DoubleClick(sender As Object, e As EventArgs) Handles lvwEnchufes.DoubleClick
        Dim lvi As ListViewItem = Me.lvwEnchufes.SelectedItems(0)

        ' Id is lvi.SubItems.Item(0).Text
        ' And name is lvi.SubItems.Item(1).Text
        MsgBox("Usted eligió el enchufe " + lvi.SubItems.Item(1).Text, MsgBoxStyle.Information, "Enchufe #" + lvi.SubItems.Item(0).Text)
    End Sub

    Private Sub lvwEnchufes_MouseUp(sender As Object, e As MouseEventArgs) Handles lvwEnchufes.MouseUp
        'https://stackoverflow.com/questions/5506811/right-click-menu-options ♥

        If e.Button <> MouseButtons.Right Then Return

        ' TODO: Error when I click (right) in ListView that is not in Item
        Dim lvi As ListViewItem = Me.lvwEnchufes.SelectedItems(0)

        Dim idEnchufe = lvi.SubItems.Item(0).Text

        Dim cms = New ContextMenuStrip
        Dim itemPowerOn = cms.Items.Add("Prender")
        itemPowerOn.Tag = "1" + idEnchufe.ToString()
        AddHandler itemPowerOn.Click, AddressOf menuChoice
        Dim itemPowerOff = cms.Items.Add("Apagar")
        itemPowerOff.Tag = "0" + idEnchufe.ToString()
        AddHandler itemPowerOff.Click, AddressOf menuChoice
        '-- El primer número identifica si se desea apagar o prender (0 para apagar, 1 para prender), y el resto de números es el ID del enchufe
        'Ej: Si el primer numero del item es "1", es porque el encufe se desea prender, y los siguientes números al 1 serán del ID del enchufe)
        cms.Show(lvwEnchufes, e.Location)

    End Sub

    Private Sub menuChoice(ByVal sender As Object, ByVal e As EventArgs)
        'Se recive el item
        Dim item = CType(sender, ToolStripMenuItem)

        ' Esto contiene el Tag del item
        Dim itemAction = item.Tag

        'Esto es el estado a hacer, si es 0 tiene que apagar, y si es 1 tiene que prender
        Dim statusToDo = itemAction.Substring(0, 1)
        Dim idEnchufe = itemAction.Substring(1, itemAction.Length - 1)

        Dim msgBoxText As String
        Dim msgBoxTitle As String

        If statusToDo = 0 Then
            msgBoxText = "¿Estás seguro que quieres apagar el enchufe {enchufe}?"
            msgBoxTitle = "Apagar"
        Else
            msgBoxText = "¿Estás seguro que quieres prender el enchufe {enchufe}?"
            msgBoxTitle = "Prender"
        End If

        MsgBox(msgBoxText.Replace("{enchufe}", idEnchufe), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, msgBoxTitle + " enchufe")

    End Sub
End Class