﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewDetailsPersona
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewDetailsPersona))
        Me.pcxIconHouse = New System.Windows.Forms.PictureBox()
        Me.lblHouseId = New System.Windows.Forms.Label()
        Me.lblNameClient = New System.Windows.Forms.Label()
        Me.lblSurnameClient = New System.Windows.Forms.Label()
        Me.lblBirthdayClient = New System.Windows.Forms.Label()
        Me.lblEmailClient = New System.Windows.Forms.Label()
        Me.lblModifyName = New System.Windows.Forms.Label()
        Me.lblModifySurname = New System.Windows.Forms.Label()
        Me.lblModifyBirthday = New System.Windows.Forms.Label()
        Me.lblModifyEmail = New System.Windows.Forms.Label()
        Me.lblModifyGender = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lvwEnchufes = New System.Windows.Forms.ListView()
        Me.idEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.nameEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.consumoEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.stateEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gbxEnchufes = New System.Windows.Forms.GroupBox()
        Me.gbxInfoClient = New System.Windows.Forms.GroupBox()
        CType(Me.pcxIconHouse, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEnchufes.SuspendLayout()
        Me.gbxInfoClient.SuspendLayout()
        Me.SuspendLayout()
        '
        'pcxIconHouse
        '
        Me.pcxIconHouse.Image = Global.frmStar_t.My.Resources.Resources.home_icon
        Me.pcxIconHouse.Location = New System.Drawing.Point(-3, 10)
        Me.pcxIconHouse.Name = "pcxIconHouse"
        Me.pcxIconHouse.Size = New System.Drawing.Size(1159, 207)
        Me.pcxIconHouse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pcxIconHouse.TabIndex = 0
        Me.pcxIconHouse.TabStop = False
        '
        'lblHouseId
        '
        Me.lblHouseId.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHouseId.Location = New System.Drawing.Point(-1, 226)
        Me.lblHouseId.Name = "lblHouseId"
        Me.lblHouseId.Size = New System.Drawing.Size(1157, 31)
        Me.lblHouseId.TabIndex = 1
        Me.lblHouseId.Text = "#"
        Me.lblHouseId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNameClient
        '
        Me.lblNameClient.AutoSize = True
        Me.lblNameClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNameClient.Location = New System.Drawing.Point(20, 21)
        Me.lblNameClient.Name = "lblNameClient"
        Me.lblNameClient.Size = New System.Drawing.Size(60, 16)
        Me.lblNameClient.TabIndex = 2
        Me.lblNameClient.Text = "Nombre:"
        '
        'lblSurnameClient
        '
        Me.lblSurnameClient.AutoSize = True
        Me.lblSurnameClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurnameClient.Location = New System.Drawing.Point(20, 52)
        Me.lblSurnameClient.Name = "lblSurnameClient"
        Me.lblSurnameClient.Size = New System.Drawing.Size(61, 16)
        Me.lblSurnameClient.TabIndex = 3
        Me.lblSurnameClient.Text = "Apellido:"
        '
        'lblBirthdayClient
        '
        Me.lblBirthdayClient.AutoSize = True
        Me.lblBirthdayClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthdayClient.Location = New System.Drawing.Point(20, 114)
        Me.lblBirthdayClient.Name = "lblBirthdayClient"
        Me.lblBirthdayClient.Size = New System.Drawing.Size(136, 16)
        Me.lblBirthdayClient.TabIndex = 4
        Me.lblBirthdayClient.Text = "Fecha de nacimiento:"
        '
        'lblEmailClient
        '
        Me.lblEmailClient.AutoSize = True
        Me.lblEmailClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmailClient.Location = New System.Drawing.Point(20, 148)
        Me.lblEmailClient.Name = "lblEmailClient"
        Me.lblEmailClient.Size = New System.Drawing.Size(49, 16)
        Me.lblEmailClient.TabIndex = 5
        Me.lblEmailClient.Text = "E-Mail:"
        '
        'lblModifyName
        '
        Me.lblModifyName.AutoSize = True
        Me.lblModifyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyName.Location = New System.Drawing.Point(77, 20)
        Me.lblModifyName.Name = "lblModifyName"
        Me.lblModifyName.Size = New System.Drawing.Size(40, 20)
        Me.lblModifyName.TabIndex = 7
        Me.lblModifyName.Text = "Otro"
        '
        'lblModifySurname
        '
        Me.lblModifySurname.AutoSize = True
        Me.lblModifySurname.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifySurname.Location = New System.Drawing.Point(79, 49)
        Me.lblModifySurname.Name = "lblModifySurname"
        Me.lblModifySurname.Size = New System.Drawing.Size(62, 20)
        Me.lblModifySurname.TabIndex = 8
        Me.lblModifySurname.Text = "Alguien"
        '
        'lblModifyBirthday
        '
        Me.lblModifyBirthday.AutoSize = True
        Me.lblModifyBirthday.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyBirthday.Location = New System.Drawing.Point(152, 113)
        Me.lblModifyBirthday.Name = "lblModifyBirthday"
        Me.lblModifyBirthday.Size = New System.Drawing.Size(89, 20)
        Me.lblModifyBirthday.TabIndex = 9
        Me.lblModifyBirthday.Text = "01/01/2001"
        '
        'lblModifyEmail
        '
        Me.lblModifyEmail.AutoSize = True
        Me.lblModifyEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyEmail.Location = New System.Drawing.Point(65, 145)
        Me.lblModifyEmail.Name = "lblModifyEmail"
        Me.lblModifyEmail.Size = New System.Drawing.Size(201, 20)
        Me.lblModifyEmail.TabIndex = 10
        Me.lblModifyEmail.Text = "otro.alguien@cualquiera.uy"
        '
        'lblModifyGender
        '
        Me.lblModifyGender.AutoSize = True
        Me.lblModifyGender.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyGender.Location = New System.Drawing.Point(71, 80)
        Me.lblModifyGender.Name = "lblModifyGender"
        Me.lblModifyGender.Size = New System.Drawing.Size(19, 20)
        Me.lblModifyGender.TabIndex = 12
        Me.lblModifyGender.Text = "F"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(20, 81)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 16)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Género:"
        '
        'lvwEnchufes
        '
        Me.lvwEnchufes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idEnchufe, Me.nameEnchufe, Me.consumoEnchufe, Me.stateEnchufe})
        Me.lvwEnchufes.FullRowSelect = True
        Me.lvwEnchufes.Location = New System.Drawing.Point(18, 19)
        Me.lvwEnchufes.Name = "lvwEnchufes"
        Me.lvwEnchufes.Size = New System.Drawing.Size(546, 257)
        Me.lvwEnchufes.TabIndex = 13
        Me.lvwEnchufes.UseCompatibleStateImageBehavior = False
        Me.lvwEnchufes.View = System.Windows.Forms.View.Details
        '
        'idEnchufe
        '
        Me.idEnchufe.Text = "ID"
        Me.idEnchufe.Width = 81
        '
        'nameEnchufe
        '
        Me.nameEnchufe.Text = "Nombre"
        Me.nameEnchufe.Width = 185
        '
        'consumoEnchufe
        '
        Me.consumoEnchufe.Text = "Consumo (este mes)"
        Me.consumoEnchufe.Width = 110
        '
        'stateEnchufe
        '
        Me.stateEnchufe.Text = "Estado"
        Me.stateEnchufe.Width = 144
        '
        'gbxEnchufes
        '
        Me.gbxEnchufes.Controls.Add(Me.lvwEnchufes)
        Me.gbxEnchufes.Location = New System.Drawing.Point(488, 260)
        Me.gbxEnchufes.Name = "gbxEnchufes"
        Me.gbxEnchufes.Size = New System.Drawing.Size(592, 295)
        Me.gbxEnchufes.TabIndex = 14
        Me.gbxEnchufes.TabStop = False
        Me.gbxEnchufes.Text = "Enchufes"
        '
        'gbxInfoClient
        '
        Me.gbxInfoClient.Controls.Add(Me.lblNameClient)
        Me.gbxInfoClient.Controls.Add(Me.lblModifySurname)
        Me.gbxInfoClient.Controls.Add(Me.lblSurnameClient)
        Me.gbxInfoClient.Controls.Add(Me.lblModifyBirthday)
        Me.gbxInfoClient.Controls.Add(Me.lblModifyGender)
        Me.gbxInfoClient.Controls.Add(Me.lblModifyName)
        Me.gbxInfoClient.Controls.Add(Me.lblBirthdayClient)
        Me.gbxInfoClient.Controls.Add(Me.lblModifyEmail)
        Me.gbxInfoClient.Controls.Add(Me.Label6)
        Me.gbxInfoClient.Controls.Add(Me.lblEmailClient)
        Me.gbxInfoClient.Location = New System.Drawing.Point(127, 260)
        Me.gbxInfoClient.Name = "gbxInfoClient"
        Me.gbxInfoClient.Size = New System.Drawing.Size(337, 228)
        Me.gbxInfoClient.TabIndex = 16
        Me.gbxInfoClient.TabStop = False
        Me.gbxInfoClient.Text = "Datos del cliente"
        '
        'frmviewDetailsHouse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1157, 580)
        Me.Controls.Add(Me.gbxInfoClient)
        Me.Controls.Add(Me.gbxEnchufes)
        Me.Controls.Add(Me.lblHouseId)
        Me.Controls.Add(Me.pcxIconHouse)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmviewDetailsHouse"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "viewDetailsHouse"
        CType(Me.pcxIconHouse, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEnchufes.ResumeLayout(False)
        Me.gbxInfoClient.ResumeLayout(False)
        Me.gbxInfoClient.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pcxIconHouse As PictureBox
    Friend WithEvents lblHouseId As Label
    Friend WithEvents lblNameClient As Label
    Friend WithEvents lblSurnameClient As Label
    Friend WithEvents lblBirthdayClient As Label
    Friend WithEvents lblEmailClient As Label
    Friend WithEvents lblModifyName As Label
    Friend WithEvents lblModifySurname As Label
    Friend WithEvents lblModifyBirthday As Label
    Friend WithEvents lblModifyEmail As Label
    Friend WithEvents lblModifyGender As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lvwEnchufes As ListView
    Friend WithEvents idEnchufe As ColumnHeader
    Friend WithEvents nameEnchufe As ColumnHeader
    Friend WithEvents stateEnchufe As ColumnHeader
    Friend WithEvents consumoEnchufe As ColumnHeader
    Friend WithEvents gbxEnchufes As GroupBox
    Friend WithEvents gbxInfoClient As GroupBox
End Class
