﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewAdmin
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewAdmin))
        Me.gbxCard = New System.Windows.Forms.GroupBox()
        Me.gbxPerson = New System.Windows.Forms.GroupBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtSurname = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtDateBirthday = New System.Windows.Forms.TextBox()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.lblSurname = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rbtGenderUndefined = New System.Windows.Forms.RadioButton()
        Me.rbtGenderM = New System.Windows.Forms.RadioButton()
        Me.rbtGenderF = New System.Windows.Forms.RadioButton()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.rbtHermanoMenor = New System.Windows.Forms.RadioButton()
        Me.rbtHermanoMayor = New System.Windows.Forms.RadioButton()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.gbxCard.SuspendLayout()
        Me.gbxPerson.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCard
        '
        Me.gbxCard.Controls.Add(Me.Panel2)
        Me.gbxCard.Location = New System.Drawing.Point(7, 352)
        Me.gbxCard.Name = "gbxCard"
        Me.gbxCard.Size = New System.Drawing.Size(199, 74)
        Me.gbxCard.TabIndex = 110
        Me.gbxCard.TabStop = False
        Me.gbxCard.Text = "Tipo de Administrador"
        '
        'gbxPerson
        '
        Me.gbxPerson.Controls.Add(Me.gbxCard)
        Me.gbxPerson.Controls.Add(Me.txtName)
        Me.gbxPerson.Controls.Add(Me.txtSurname)
        Me.gbxPerson.Controls.Add(Me.lblName)
        Me.gbxPerson.Controls.Add(Me.txtDateBirthday)
        Me.gbxPerson.Controls.Add(Me.lblBirthday)
        Me.gbxPerson.Controls.Add(Me.lblSurname)
        Me.gbxPerson.Controls.Add(Me.txtPhone)
        Me.gbxPerson.Controls.Add(Me.lblPhone)
        Me.gbxPerson.Controls.Add(Me.lblGender)
        Me.gbxPerson.Controls.Add(Me.txtEmail)
        Me.gbxPerson.Controls.Add(Me.Panel1)
        Me.gbxPerson.Controls.Add(Me.lblEmail)
        Me.gbxPerson.Location = New System.Drawing.Point(49, 12)
        Me.gbxPerson.Name = "gbxPerson"
        Me.gbxPerson.Size = New System.Drawing.Size(259, 439)
        Me.gbxPerson.TabIndex = 109
        Me.gbxPerson.TabStop = False
        Me.gbxPerson.Text = "Datos de la persona"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(7, 45)
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtName.Size = New System.Drawing.Size(220, 20)
        Me.txtName.TabIndex = 2
        '
        'txtSurname
        '
        Me.txtSurname.Location = New System.Drawing.Point(7, 92)
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSurname.Size = New System.Drawing.Size(220, 20)
        Me.txtSurname.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(6, 23)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(220, 23)
        Me.lblName.TabIndex = 102
        Me.lblName.Text = "Nombre"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDateBirthday
        '
        Me.txtDateBirthday.Location = New System.Drawing.Point(7, 312)
        Me.txtDateBirthday.Name = "txtDateBirthday"
        Me.txtDateBirthday.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDateBirthday.Size = New System.Drawing.Size(220, 20)
        Me.txtDateBirthday.TabIndex = 9
        '
        'lblBirthday
        '
        Me.lblBirthday.Location = New System.Drawing.Point(6, 287)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(220, 23)
        Me.lblBirthday.TabIndex = 107
        Me.lblBirthday.Text = "Fecha de nacimiento"
        Me.lblBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSurname
        '
        Me.lblSurname.Location = New System.Drawing.Point(7, 70)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(220, 23)
        Me.lblSurname.TabIndex = 103
        Me.lblSurname.Text = "Apellido"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(7, 261)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPhone.Size = New System.Drawing.Size(220, 20)
        Me.txtPhone.TabIndex = 8
        '
        'lblPhone
        '
        Me.lblPhone.Location = New System.Drawing.Point(7, 238)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(220, 23)
        Me.lblPhone.TabIndex = 106
        Me.lblPhone.Text = "Teléfono celular"
        Me.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGender
        '
        Me.lblGender.Location = New System.Drawing.Point(7, 117)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(220, 23)
        Me.lblGender.TabIndex = 104
        Me.lblGender.Text = "Género"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(7, 211)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtEmail.Size = New System.Drawing.Size(220, 20)
        Me.txtEmail.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rbtGenderUndefined)
        Me.Panel1.Controls.Add(Me.rbtGenderM)
        Me.Panel1.Controls.Add(Me.rbtGenderF)
        Me.Panel1.Location = New System.Drawing.Point(7, 141)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(184, 38)
        Me.Panel1.TabIndex = 21
        '
        'rbtGenderUndefined
        '
        Me.rbtGenderUndefined.AutoSize = True
        Me.rbtGenderUndefined.Location = New System.Drawing.Point(88, 10)
        Me.rbtGenderUndefined.Name = "rbtGenderUndefined"
        Me.rbtGenderUndefined.Size = New System.Drawing.Size(94, 17)
        Me.rbtGenderUndefined.TabIndex = 6
        Me.rbtGenderUndefined.TabStop = True
        Me.rbtGenderUndefined.Text = "Sin especificar"
        Me.rbtGenderUndefined.UseVisualStyleBackColor = True
        '
        'rbtGenderM
        '
        Me.rbtGenderM.AutoSize = True
        Me.rbtGenderM.Location = New System.Drawing.Point(9, 10)
        Me.rbtGenderM.Name = "rbtGenderM"
        Me.rbtGenderM.Size = New System.Drawing.Size(34, 17)
        Me.rbtGenderM.TabIndex = 4
        Me.rbtGenderM.TabStop = True
        Me.rbtGenderM.Text = "M"
        Me.rbtGenderM.UseVisualStyleBackColor = True
        '
        'rbtGenderF
        '
        Me.rbtGenderF.AutoSize = True
        Me.rbtGenderF.Location = New System.Drawing.Point(50, 10)
        Me.rbtGenderF.Name = "rbtGenderF"
        Me.rbtGenderF.Size = New System.Drawing.Size(31, 17)
        Me.rbtGenderF.TabIndex = 5
        Me.rbtGenderF.TabStop = True
        Me.rbtGenderF.Text = "F"
        Me.rbtGenderF.UseVisualStyleBackColor = True
        '
        'lblEmail
        '
        Me.lblEmail.Location = New System.Drawing.Point(6, 188)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(220, 23)
        Me.lblEmail.TabIndex = 105
        Me.lblEmail.Text = "E-mail"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.rbtHermanoMenor)
        Me.Panel2.Controls.Add(Me.rbtHermanoMayor)
        Me.Panel2.Location = New System.Drawing.Point(12, 25)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(144, 38)
        Me.Panel2.TabIndex = 22
        '
        'rbtHermanoMenor
        '
        Me.rbtHermanoMenor.AutoSize = True
        Me.rbtHermanoMenor.Location = New System.Drawing.Point(9, 10)
        Me.rbtHermanoMenor.Name = "rbtHermanoMenor"
        Me.rbtHermanoMenor.Size = New System.Drawing.Size(55, 17)
        Me.rbtHermanoMenor.TabIndex = 4
        Me.rbtHermanoMenor.TabStop = True
        Me.rbtHermanoMenor.Text = "Menor"
        Me.rbtHermanoMenor.UseVisualStyleBackColor = True
        '
        'rbtHermanoMayor
        '
        Me.rbtHermanoMayor.AutoSize = True
        Me.rbtHermanoMayor.Location = New System.Drawing.Point(70, 10)
        Me.rbtHermanoMayor.Name = "rbtHermanoMayor"
        Me.rbtHermanoMayor.Size = New System.Drawing.Size(54, 17)
        Me.rbtHermanoMayor.TabIndex = 5
        Me.rbtHermanoMayor.TabStop = True
        Me.rbtHermanoMayor.Text = "Mayor"
        Me.rbtHermanoMayor.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(174, 467)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(94, 23)
        Me.btnCancel.TabIndex = 111
        Me.btnCancel.Text = "&Cancelar"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(71, 467)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(94, 23)
        Me.btnSave.TabIndex = 110
        Me.btnSave.Text = "&Guardar"
        '
        'frmNewAdmin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 510)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.gbxPerson)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmNewAdmin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agregar administrador"
        Me.gbxCard.ResumeLayout(False)
        Me.gbxPerson.ResumeLayout(False)
        Me.gbxPerson.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxCard As GroupBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents rbtHermanoMenor As RadioButton
    Friend WithEvents rbtHermanoMayor As RadioButton
    Friend WithEvents gbxPerson As GroupBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtSurname As TextBox
    Friend WithEvents lblName As Label
    Friend WithEvents txtDateBirthday As TextBox
    Friend WithEvents lblBirthday As Label
    Friend WithEvents lblSurname As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents lblPhone As Label
    Friend WithEvents lblGender As Label
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents rbtGenderUndefined As RadioButton
    Friend WithEvents rbtGenderM As RadioButton
    Friend WithEvents rbtGenderF As RadioButton
    Friend WithEvents lblEmail As Label
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnSave As Button
End Class
