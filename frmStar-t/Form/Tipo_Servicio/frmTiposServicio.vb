﻿Imports System.Data.SqlClient

Public Class frmTiposServicio
    Private Sub frmTiposServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadData()
    End Sub

    Private Sub LoadData()
        Dim oTipoServicio As New TipoServicio()

        Dim sqlResult As SqlDataReader = oTipoServicio.obtenerListado()

        lvwTiposServicio.Items.Clear()

        If sqlResult.HasRows Then
            While sqlResult.Read
                Dim lvItem As ListViewItem = lvwTiposServicio.Items.Add(sqlResult("id").ToString)
                lvItem.SubItems.Add(sqlResult("nombre").ToString)
                lvItem.SubItems.Add(sqlResult("descripcion").ToString)
                lvItem.SubItems.Add(sqlResult("precio").ToString)

            End While
        End If
        sqlResult.Close()

    End Sub

    Private Sub btnOpenFormAddTipoServicio_Click(sender As Object, e As EventArgs) Handles btnOpenFormAddTipoServicio.Click
        frmAddTipoServicio.lblToDo.Text = "Add"
        frmAddTipoServicio.Show()
    End Sub

    Private Sub frmTiposServicio_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        LoadData()
    End Sub

    Private Sub lvwTiposServicio_MouseUp(sender As Object, e As MouseEventArgs) Handles lvwTiposServicio.MouseUp
        If e.Button <> MouseButtons.Right Then Return

        ' TODO: Error when I click (right) in ListView that is not in Item
        Try

            Dim lvi As ListViewItem = Me.lvwTiposServicio.SelectedItems(0)

            Dim idTipoServicio = lvi.SubItems.Item(0).Text

            Dim nameTipoServicio = lvi.SubItems.Item(1).Text

            Dim descripcionTipoServicio = lvi.SubItems.Item(2).Text

            Dim precioTipoServicio = lvi.SubItems.Item(3).Text

            Dim oTipoServicio As New TipoServicio(idTipoServicio, nameTipoServicio, descripcionTipoServicio, precioTipoServicio)

            Dim cms = New ContextMenuStrip
            Dim itemUpdate = cms.Items.Add("Editar")
            itemUpdate.Tag = oTipoServicio
            AddHandler itemUpdate.Click, AddressOf menuChoice
            Dim itemDelete = cms.Items.Add("Eliminar")
            itemDelete.Tag = oTipoServicio
            AddHandler itemDelete.Click, AddressOf menuChoice

            cms.Show(lvwTiposServicio, e.Location)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub menuChoice(ByVal sender As Object, ByVal e As EventArgs)
        'Se recive el item
        Dim item = CType(sender, ToolStripMenuItem)

        ' Esto contiene el Tag del item
        Dim oTipoServicio = item.Tag

        Dim msgBoxText As String
        Dim msgBoxTitle As String

        If item.Text = "Eliminar" Then
            msgBoxText = "¿Estás seguro que quieres eliminar el tipo de servicio {tipo_servicio}?"
            msgBoxTitle = "Eliminar"

            Dim resultAction As Integer = MsgBox(msgBoxText.Replace("{tipo_servicio}", oTipoServicio.Nombre), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, msgBoxTitle + " enchufe")

            If (resultAction = 6) Then
                lvwTiposServicio.Items.Remove(lvwTiposServicio.SelectedItems(0))
                oTipoServicio.Borrar(oTipoServicio.id)
                MsgBox("Se borró exitosamente el tipo de servicio " + oTipoServicio.nombre)
            End If
        Else

            frmAddTipoServicio.lblToDo.Text = "Edit"
            frmAddTipoServicio.lblIdTipoServicio.Text = oTipoServicio.id
            frmAddTipoServicio.txtName.Text = oTipoServicio.nombre
            frmAddTipoServicio.txtDescription.Text = oTipoServicio.descripcion
            frmAddTipoServicio.txtPrice.Text = oTipoServicio.precio

            frmAddTipoServicio.Show()
        End If

    End Sub

End Class