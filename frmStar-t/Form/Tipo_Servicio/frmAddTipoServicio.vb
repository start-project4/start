﻿Public Class frmAddTipoServicio
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim name, price As String
        Dim priceInteger As Integer

        name = Trim(txtName.Text)
        price = Trim(txtPrice.Text)

        If lblToDo.Text = "Add" Then

            If Name = "" Then
                'Si el usr está vacío, muestro error
                MsgBox("Debes ingresar un nombre", MsgBoxStyle.Exclamation, "Error")
            ElseIf Not Int32.TryParse(price, priceInteger) Then
                'Si el pwd está vacío, muestro error
                MsgBox("Debes ingresar un precio válido", MsgBoxStyle.Exclamation, "Error")
            Else
                'LLamar a función para agregar tipos de servicios

                Dim oTipoServicio As New TipoServicio(Name, txtDescription.Text, priceInteger)

                If oTipoServicio.Insertar() = 1 Then
                    MsgBox("Se agregó el tipo de servicio " + Name, MsgBoxStyle.Exclamation, "Éxito")

                    Dim newItem As New ListViewItem(" ")

                    newItem.SubItems.Add(Name)

                    newItem.SubItems.Add(txtDescription.Text)

                    newItem.SubItems.Add(price)

                    frmTiposServicio.lvwTiposServicio.Items.Add(newItem)
                End If
                frmTiposServicio.Close()
                frmTiposServicio.Show()
                'Me.Close()
            End If

        Else
            Dim idTipoServicio As Integer = Convert.ToInt32(lblIdTipoServicio.Text)
            Dim oTipoServicio As New TipoServicio(idTipoServicio, name, txtDescription.Text, price)

            oTipoServicio.Editar()
            MsgBox("Se editó el tipo de servicio " + name, MsgBoxStyle.Exclamation, "Éxito")
            Me.Close()

        End If
    End Sub

    Private Sub frmAddTipoServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If lblToDo.Text = "Add" Then
            btnAdd.Text = "Añadir"
        Else
            btnAdd.Text = "Editar"
        End If
    End Sub
End Class