﻿Public Class frmAddEnchufe
    Private Sub frmAddEnchufe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If lblToDo.Text = "Add" Then
            btnAdd.Text = "Añadir"
        Else
            btnAdd.Text = "Editar"
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If lblToDo.Text = "Add" Then
            Dim statusOrEnchufe = validation()

            If Not TypeOf statusOrEnchufe Is Boolean Then
                Dim oEnchufe = statusOrEnchufe

                If oEnchufe.Insertar() = 1 Then
                    MsgBox("Se agregó el enchufe" + oEnchufe.nombre, MsgBoxStyle.Exclamation, "Éxito")
                    frmViewEnchufess.Close()
                    frmViewEnchufess.Show()
                End If
                Me.Close()
            End If
        Else
            Dim idEnchufe As Integer = Convert.ToInt32(lblIdEnchufe.Text)

            Dim statusOrEnchufe = validation()

            If Not TypeOf statusOrEnchufe Is Boolean Then
                Dim oEnchufe = statusOrEnchufe

                oEnchufe.Id = idEnchufe

                oEnchufe.Editar()
                MsgBox("Se editó el enchufe" + oEnchufe.nombre, MsgBoxStyle.Exclamation, "Éxito")
                'frmViewEnchufess.Close()
                'frmViewEnchufess.Show()
                Me.Close()
            End If
        End If

    End Sub

    Function validation()
        Dim status As Boolean = False

        Dim name, date_compra, password, resumen As String
        Dim idTipoServicio, idCliente As Integer

        name = Trim(txtName.Text)
        date_compra = Trim(txtDate.Text)
        password = Trim(txtPassword.Text)

        If name = "" Then
            MsgBox("Debes ingresar un nombre", MsgBoxStyle.Exclamation, "Error")
        ElseIf date_compra = "" Then
            MsgBox("Debes ingresar una fecha de compra (YYYY-MM-DD)", MsgBoxStyle.Exclamation, "Error")
        ElseIf password = "" Then
            MsgBox("Debes ingresar una contraseña", MsgBoxStyle.Exclamation, "Error")
        Else
            If txtIdTipoServicio.Text <> "" Then
                status = True
                If Not Int32.TryParse(txtIdTipoServicio.Text, idTipoServicio) Then
                    status = False
                    MsgBox("El ID del Tipo de Servicio debe ser válido", MsgBoxStyle.Exclamation, "Error")
                End If
            End If
            If txtIdCliente.Text <> "" Then
                status = True
                If Not Int32.TryParse(txtIdCliente.Text, idCliente) Then
                    status = False
                    MsgBox("El ID del cliente debe ser válido", MsgBoxStyle.Exclamation, "Error")
                End If
                status = True
            End If
        End If
        resumen = txtResumen.Text

        If status Then
            Dim oEnchufe As New Enchufe
            If lblToDo.Text = "Add" Then
                oEnchufe = New Enchufe(name, date_compra, password, idCliente, idTipoServicio, resumen)
            Else
                Dim idEnchufe As Integer = Convert.ToInt32(lblIdEnchufe.Text)
                oEnchufe = New Enchufe(idEnchufe, name, date_compra, password, idCliente, idTipoServicio, resumen)
            End If

            Return oEnchufe
        Else
            Return status
        End If

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class