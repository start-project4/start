﻿Imports System.Data.SqlClient

Public Class frmViewEnchufess
    Private Sub frmViewEnchufess_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Width = Screen.PrimaryScreen.WorkingArea.Width
        Me.Height = Screen.PrimaryScreen.WorkingArea.Height
        lvwEnchufes.Width = Me.Width
        lvwEnchufes.Height = Me.Height

        btnOpenFormAddEnchufe.Location = New Point(Me.Width - btnOpenFormAddEnchufe.Width - 40, 8)
        btnReload.Location = New Point(Me.Width - btnReload.Width - 40, 38)
        LoadData()
    End Sub

    Private Sub LoadData()
        Dim oEnchufe As New Enchufe()

        Dim sqlResult As SqlDataReader = oEnchufe.obtenerListado()

        lvwEnchufes.Items.Clear()

        If sqlResult.HasRows Then
            While sqlResult.Read
                Dim lvItem As ListViewItem = lvwEnchufes.Items.Add(sqlResult("id").ToString)
                lvItem.SubItems.Add(sqlResult("nombre").ToString())
                lvItem.SubItems.Add(sqlResult("estado").ToString())
                lvItem.SubItems.Add(sqlResult("icono").ToString())
                lvItem.SubItems.Add(sqlResult("contrasenia").ToString())
                lvItem.SubItems.Add(sqlResult("fecha_compra").ToString())
                lvItem.SubItems.Add(sqlResult("id_Tipo_Servicio").ToString())
                lvItem.SubItems.Add(sqlResult("nombreCliente").ToString() + " - #" + sqlResult("cliente_id").ToString())
                lvItem.SubItems.Add(sqlResult("resumen").ToString())
            End While
        End If
        sqlResult.Close()

    End Sub

    Private Sub btnOpenFormAddEnchufe_Click(sender As Object, e As EventArgs) Handles btnOpenFormAddEnchufe.Click
        frmAddEnchufe.lblToDo.Text = "Add"
        frmAddEnchufe.Show()
    End Sub

    Private Sub lvwEnchufes_MouseUp(sender As Object, e As MouseEventArgs) Handles lvwEnchufes.MouseUp
        If e.Button <> MouseButtons.Right Then Return
        Try
            Dim lvi As ListViewItem = Me.lvwEnchufes.SelectedItems(0)

            Dim oEnchufe As Enchufe = getEnchufeByListItem(lvi)

            Dim cms = New ContextMenuStrip
            Dim itemUpdate = cms.Items.Add("Editar")
            itemUpdate.Tag = oEnchufe
            AddHandler itemUpdate.Click, AddressOf menuChoice
            Dim itemDelete = cms.Items.Add("Eliminar")
            itemDelete.Tag = oEnchufe
            AddHandler itemDelete.Click, AddressOf menuChoice

            cms.Show(lvwEnchufes, e.Location)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub menuChoice(ByVal sender As Object, ByVal e As EventArgs)
        'Se recive el item
        Dim item = CType(sender, ToolStripMenuItem)

        ' Esto contiene el Tag del item
        Dim oEnchufe = item.Tag

        Dim msgBoxText As String
        Dim msgBoxTitle As String

        If item.Text = "Eliminar" Then
            msgBoxText = "¿Estás seguro que quieres eliminar el enchufe {nombre_enchufe}?"
            msgBoxTitle = "Eliminar"

            Dim resultAction As Integer = MsgBox(msgBoxText.Replace("{nombre_enchufe}", oEnchufe.Nombre), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, msgBoxTitle + " enchufe")

            If (resultAction = 6) Then
                lvwEnchufes.Items.Remove(lvwEnchufes.SelectedItems(0))
                oEnchufe.Borrar(oEnchufe.id)
                MsgBox("Se borró exitosamente el enchufe " + oEnchufe.nombre)
            End If
        Else

            'TODO: Tiene que abrir el formulario de "abrir", en este caso, editar, con los atributos correspondientes
            frmAddEnchufe.lblToDo.Text = "Edit"
            frmAddEnchufe.lblIdEnchufe.Text = oEnchufe.id
            frmAddEnchufe.txtName.Text = oEnchufe.nombre
            frmAddEnchufe.txtDate.Text = oEnchufe.Fecha_Compra
            frmAddEnchufe.txtPassword.Text = oEnchufe.Contrasenia
            If oEnchufe.Id_Cliente <> 0 Then
                frmAddEnchufe.txtIdCliente.Text = oEnchufe.Id_Cliente
            End If
            If oEnchufe.Id_Tipo_Servicio <> 0 Then
                frmAddEnchufe.txtIdTipoServicio.Text = oEnchufe.Id_Tipo_Servicio
            End If

            frmAddEnchufe.txtResumen.Text = oEnchufe.resumen

            frmAddEnchufe.Show()
        End If

    End Sub

    Private Sub btnReload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
        LoadData()
    End Sub

    Private Sub lvwEnchufes_DoubleClick(sender As Object, e As EventArgs) Handles lvwEnchufes.DoubleClick
        Try
            Dim lvi As ListViewItem = Me.lvwEnchufes.SelectedItems(0)

            Dim oEnchufe As Enchufe = getEnchufeByListItem(lvi)

            oEnchufe.Estado = Not (oEnchufe.Estado)

            oEnchufe.Editar()

        Catch ex As Exception
            MsgBox("Aprete sobre algún enchufe")
        End Try
    End Sub

    Function getEnchufeByListItem(lvi As ListViewItem)
        Dim idEnchufe = Integer.Parse(lvi.SubItems.Item(0).Text)

        Dim nameEnchufe = lvi.SubItems.Item(1).Text

        Dim estadoEnchufe = Convert.ToBoolean(lvi.SubItems.Item(2).Text)

        Dim iconoEnchufe = lvi.SubItems.Item(3).Text

        Dim passwordEnchufe = lvi.SubItems.Item(4).Text

        Dim fechaCompra = lvi.SubItems.Item(5).Text

        Dim idTipoServicio = Nothing
        If (lvi.SubItems.Item(6).Text <> "") Then
            idTipoServicio = Integer.Parse(lvi.SubItems.Item(6).Text)
        End If

        Dim idCliente As String = lvi.SubItems.Item(7).Text.Split(New Char() {"#"c})(1)

        Dim intIdCliente = Nothing

        If idCliente <> "" Then
            intIdCliente = Integer.Parse(idCliente)
        End If

        Dim resumen = lvi.SubItems.Item(8).Text

        Dim oEnchufe As New Enchufe(idEnchufe, nameEnchufe, fechaCompra, estadoEnchufe, iconoEnchufe, resumen, passwordEnchufe, idTipoServicio, intIdCliente)

        Return oEnchufe
    End Function
End Class