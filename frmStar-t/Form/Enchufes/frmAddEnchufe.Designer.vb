﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddEnchufe
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtResumen = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.PasswordLabel = New System.Windows.Forms.Label()
        Me.UsernameLabel = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblToDo = New System.Windows.Forms.Label()
        Me.lblIdEnchufe = New System.Windows.Forms.Label()
        Me.txtIdCliente = New System.Windows.Forms.TextBox()
        Me.idCliente = New System.Windows.Forms.Label()
        Me.txtIdTipoServicio = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtDate
        '
        Me.txtDate.Location = New System.Drawing.Point(28, 79)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(220, 20)
        Me.txtDate.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(26, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(220, 23)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "&Fecha de compra (YYYY-MM-DD)*"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(138, 350)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(94, 23)
        Me.btnCancel.TabIndex = 17
        Me.btnCancel.Text = "&Cancelar"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(35, 350)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(94, 23)
        Me.btnAdd.TabIndex = 16
        Me.btnAdd.Text = "&Añadir"
        '
        'txtResumen
        '
        Me.txtResumen.Location = New System.Drawing.Point(28, 265)
        Me.txtResumen.Multiline = True
        Me.txtResumen.Name = "txtResumen"
        Me.txtResumen.Size = New System.Drawing.Size(220, 64)
        Me.txtResumen.TabIndex = 14
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(28, 30)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(220, 20)
        Me.txtName.TabIndex = 13
        '
        'PasswordLabel
        '
        Me.PasswordLabel.Location = New System.Drawing.Point(26, 245)
        Me.PasswordLabel.Name = "PasswordLabel"
        Me.PasswordLabel.Size = New System.Drawing.Size(220, 23)
        Me.PasswordLabel.TabIndex = 19
        Me.PasswordLabel.Text = "&Resumen"
        Me.PasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UsernameLabel
        '
        Me.UsernameLabel.Location = New System.Drawing.Point(26, 10)
        Me.UsernameLabel.Name = "UsernameLabel"
        Me.UsernameLabel.Size = New System.Drawing.Size(220, 23)
        Me.UsernameLabel.TabIndex = 18
        Me.UsernameLabel.Text = "&Nombre*"
        Me.UsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(29, 124)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(220, 20)
        Me.txtPassword.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(27, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(220, 23)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "&Contraseña*"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToDo
        '
        Me.lblToDo.AutoSize = True
        Me.lblToDo.Location = New System.Drawing.Point(248, 284)
        Me.lblToDo.Name = "lblToDo"
        Me.lblToDo.Size = New System.Drawing.Size(0, 13)
        Me.lblToDo.TabIndex = 23
        Me.lblToDo.Visible = False
        '
        'lblIdEnchufe
        '
        Me.lblIdEnchufe.AutoSize = True
        Me.lblIdEnchufe.Location = New System.Drawing.Point(254, 279)
        Me.lblIdEnchufe.Name = "lblIdEnchufe"
        Me.lblIdEnchufe.Size = New System.Drawing.Size(0, 13)
        Me.lblIdEnchufe.TabIndex = 24
        Me.lblIdEnchufe.Visible = False
        '
        'txtIdCliente
        '
        Me.txtIdCliente.Location = New System.Drawing.Point(29, 172)
        Me.txtIdCliente.Name = "txtIdCliente"
        Me.txtIdCliente.Size = New System.Drawing.Size(220, 20)
        Me.txtIdCliente.TabIndex = 25
        '
        'idCliente
        '
        Me.idCliente.Location = New System.Drawing.Point(27, 152)
        Me.idCliente.Name = "idCliente"
        Me.idCliente.Size = New System.Drawing.Size(220, 23)
        Me.idCliente.TabIndex = 26
        Me.idCliente.Text = "ID del cliente"
        Me.idCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIdTipoServicio
        '
        Me.txtIdTipoServicio.Location = New System.Drawing.Point(29, 220)
        Me.txtIdTipoServicio.Name = "txtIdTipoServicio"
        Me.txtIdTipoServicio.Size = New System.Drawing.Size(220, 20)
        Me.txtIdTipoServicio.TabIndex = 27
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(27, 200)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(220, 23)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "ID Tipo Servicio*"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmAddEnchufe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 378)
        Me.Controls.Add(Me.txtIdTipoServicio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtIdCliente)
        Me.Controls.Add(Me.idCliente)
        Me.Controls.Add(Me.lblIdEnchufe)
        Me.Controls.Add(Me.lblToDo)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.txtResumen)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.PasswordLabel)
        Me.Controls.Add(Me.UsernameLabel)
        Me.Name = "frmAddEnchufe"
        Me.Text = "Añadir enchufe"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtDate As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents txtResumen As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents PasswordLabel As Label
    Friend WithEvents UsernameLabel As Label
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents lblToDo As Label
    Friend WithEvents lblIdEnchufe As Label
    Friend WithEvents txtIdCliente As TextBox
    Friend WithEvents idCliente As Label
    Friend WithEvents txtIdTipoServicio As TextBox
    Friend WithEvents Label3 As Label
End Class
