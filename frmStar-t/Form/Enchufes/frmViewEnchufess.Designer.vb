﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewEnchufess
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwEnchufes = New System.Windows.Forms.ListView()
        Me.idEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.nombreEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.estadoEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.iconoEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.contraseniaEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.fechaCompraEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idTipoServicioEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idClienteEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.resumenEnchufe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnOpenFormAddEnchufe = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnReload = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lvwEnchufes
        '
        Me.lvwEnchufes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idEnchufe, Me.nombreEnchufe, Me.estadoEnchufe, Me.iconoEnchufe, Me.contraseniaEnchufe, Me.fechaCompraEnchufe, Me.idTipoServicioEnchufe, Me.idClienteEnchufe, Me.resumenEnchufe})
        Me.lvwEnchufes.FullRowSelect = True
        Me.lvwEnchufes.HideSelection = False
        Me.lvwEnchufes.Location = New System.Drawing.Point(22, 63)
        Me.lvwEnchufes.Name = "lvwEnchufes"
        Me.lvwEnchufes.Size = New System.Drawing.Size(1034, 472)
        Me.lvwEnchufes.TabIndex = 0
        Me.lvwEnchufes.UseCompatibleStateImageBehavior = False
        Me.lvwEnchufes.View = System.Windows.Forms.View.Details
        '
        'idEnchufe
        '
        Me.idEnchufe.Text = "ID"
        '
        'nombreEnchufe
        '
        Me.nombreEnchufe.Text = "Nombre"
        '
        'estadoEnchufe
        '
        Me.estadoEnchufe.Text = "Estado"
        '
        'iconoEnchufe
        '
        Me.iconoEnchufe.Text = "Icono"
        '
        'contraseniaEnchufe
        '
        Me.contraseniaEnchufe.Text = "Contraseña"
        Me.contraseniaEnchufe.Width = 100
        '
        'fechaCompraEnchufe
        '
        Me.fechaCompraEnchufe.Text = "Fecha compra"
        Me.fechaCompraEnchufe.Width = 120
        '
        'idTipoServicioEnchufe
        '
        Me.idTipoServicioEnchufe.Text = "Tipo Servicio"
        '
        'idClienteEnchufe
        '
        Me.idClienteEnchufe.Text = "Cliente"
        '
        'resumenEnchufe
        '
        Me.resumenEnchufe.Text = "Resumen"
        Me.resumenEnchufe.Width = 180
        '
        'btnOpenFormAddEnchufe
        '
        Me.btnOpenFormAddEnchufe.Location = New System.Drawing.Point(1000, 5)
        Me.btnOpenFormAddEnchufe.Name = "btnOpenFormAddEnchufe"
        Me.btnOpenFormAddEnchufe.Size = New System.Drawing.Size(75, 23)
        Me.btnOpenFormAddEnchufe.TabIndex = 2
        Me.btnOpenFormAddEnchufe.Text = "Agregar"
        Me.btnOpenFormAddEnchufe.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft YaHei", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 46)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Enchufes"
        '
        'btnReload
        '
        Me.btnReload.Location = New System.Drawing.Point(1000, 34)
        Me.btnReload.Name = "btnReload"
        Me.btnReload.Size = New System.Drawing.Size(75, 23)
        Me.btnReload.TabIndex = 4
        Me.btnReload.Text = "Recargar"
        Me.btnReload.UseVisualStyleBackColor = True
        '
        'frmViewEnchufess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1154, 545)
        Me.Controls.Add(Me.btnReload)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnOpenFormAddEnchufe)
        Me.Controls.Add(Me.lvwEnchufes)
        Me.Name = "frmViewEnchufess"
        Me.Text = "Enchufes"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lvwEnchufes As ListView
    Friend WithEvents idEnchufe As ColumnHeader
    Friend WithEvents nombreEnchufe As ColumnHeader
    Friend WithEvents estadoEnchufe As ColumnHeader
    Friend WithEvents iconoEnchufe As ColumnHeader
    Friend WithEvents contraseniaEnchufe As ColumnHeader
    Friend WithEvents fechaCompraEnchufe As ColumnHeader
    Friend WithEvents idTipoServicioEnchufe As ColumnHeader
    Friend WithEvents idClienteEnchufe As ColumnHeader
    Friend WithEvents resumenEnchufe As ColumnHeader
    Friend WithEvents btnOpenFormAddEnchufe As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btnReload As Button
End Class
