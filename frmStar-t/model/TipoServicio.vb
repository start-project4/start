﻿Imports System.Data.SqlClient

Public Class TipoServicio
    Inherits Generica

    ' Acá tipo servicio va a tener todos los atributos de la tabla tipo servicio, con el mismo tipo de datos
    Private _id As Integer
    Private _nombre As String
    Private _descripcion As String
    Private _precio As Integer

    Public Sub New()
        Initialize()
    End Sub

    Public Sub New(id As Integer, nombre As String, descripcion As String, precio As Integer)
        Me.Id = id
        Me.Nombre = nombre
        Me.Descripcion = descripcion
        Me.Precio = precio

        Initialize()
    End Sub

    Public Sub New(nombre As String, descripcion As String, precio As Integer)
        Me.Nombre = nombre
        Me.Descripcion = descripcion
        Me.Precio = precio

        Initialize()
    End Sub

    Public Sub Initialize()
        MyBase.nombreTabla = "tipo_servicio"
        MyBase.atributosInsert = {"Nombre", "Descripcion", "Precio"}
        MyBase.atributosWhere = {"Id"}
    End Sub

    Public Property Id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Public Property Precio As Integer
        Get
            Return _precio
        End Get
        Set(value As Integer)
            _precio = value
        End Set
    End Property

    Public Overrides Function obtenerListado() As SqlDataReader
        Dim results As SqlDataReader = MyBase.obtenerListado()

        Return results
    End Function

End Class
