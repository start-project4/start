﻿Imports System.Data.SqlClient

Public NotInheritable Class DBConn

    Private Shared ReadOnly _instance As New Lazy(Of DBConn) _
        (Function() New DBConn(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication)

    Private myConn As SqlConnection
    Private myCmd As SqlCommand

    Private Sub New()
        myConn = New SqlConnection("Server=192.168.1.42;Database=startdb;User=SA;Password=Start-Password")
    End Sub

    Public Shared ReadOnly Property Instance() As DBConn
        Get
            Return _instance.Value
        End Get

    End Property

    Public Function SelectStatement(sqlCmd As SqlCommand) As SqlDataReader

        OpenConnection()
        sqlCmd.Connection = myConn
        Dim sqlResult As SqlDataReader = sqlCmd.ExecuteReader()

        Return sqlResult

    End Function

    Public Function ABMStatment(sqlCmd As SqlCommand) As Integer

        OpenConnection()

        sqlCmd.Connection = myConn

        Return sqlCmd.ExecuteNonQuery()

    End Function

    Private Sub OpenConnection()
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
    End Sub

End Class