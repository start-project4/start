﻿Imports System.Data.SqlClient

Public Class Enchufe
    Inherits Generica

    ' Acá tipo servicio va a tener todos los atributos de la tabla tipo servicio, con el mismo tipo de datos
    Private _id As Integer
    Private _nombre As String
    Private _fechaCompra As String
    Private _estado As Boolean
    Private _icono As String
    Private _resumen As String
    Private _contrasenia As String
    Private _idTipoServicio As Integer
    Private _idCliente As Integer

    Public Sub New()
        Initialize()
    End Sub

    Public Sub New(id As Integer, nombre As String, fechaCompra As String, estado As Boolean, icono As String, resumen As String, contrasenia As String, idTipoServicio As Integer, idCliente As Integer)
        Me.Id = id
        Me.Nombre = nombre
        Me.Fecha_Compra = fechaCompra
        Me.Estado = estado
        Me.Icono = icono
        Me.Resumen = resumen
        Me.Contrasenia = contrasenia
        Me.Id_Tipo_Servicio = idTipoServicio
        Me.Id_Cliente = idCliente

        Initialize()
    End Sub

    Public Sub New(id As Integer, nombre As String, fechaCompra As String, contrasenia As String, idCliente As Integer, idTipoServicio As Integer, resumen As String)
        Me.Id = id
        Me.Nombre = nombre
        Me.Fecha_Compra = fechaCompra
        Me.Contrasenia = contrasenia
        Me.Id_Cliente = idCliente
        Me.Id_Tipo_Servicio = idTipoServicio
        Me.Resumen = resumen
        Me.Estado = Estado
        Me.Contrasenia = contrasenia

        Initialize()
    End Sub

    Public Sub New(nombre As String, fechaCompra As String, contrasenia As String, idCliente As Integer, idTipoServicio As Integer, resumen As String)
        Me.Nombre = nombre
        Me.Fecha_Compra = fechaCompra
        Me.Contrasenia = contrasenia
        Me.Id_Cliente = idCliente
        Me.Id_Tipo_Servicio = idTipoServicio
        Me.Resumen = resumen
        Me.Estado = Estado
        Me.Contrasenia = contrasenia

        Initialize()
    End Sub

    Public Sub Initialize()
        MyBase.nombreTabla = "enchufe"
        MyBase.atributosInsert = {"Fecha_Compra", "Resumen", "Contrasenia", "Id_Tipo_Servicio", "Id_Cliente"}
        MyBase.atributosWhere = {"Id"}

    End Sub

    Public Property Id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Fecha_Compra As String
        Get
            Return _fechaCompra
        End Get
        Set(value As String)
            _fechaCompra = value
        End Set
    End Property

    Public Property Estado As Boolean
        Get
            Return _estado
        End Get
        Set(value As Boolean)
            _estado = value
        End Set
    End Property

    Public Property Icono As String
        Get
            Return _icono
        End Get
        Set(value As String)
            _icono = value
        End Set
    End Property

    Public Property Resumen As String
        Get
            Return _resumen
        End Get
        Set(value As String)
            _resumen = value
        End Set
    End Property

    Public Property Contrasenia As String
        Get
            Return _contrasenia
        End Get
        Set(value As String)
            _contrasenia = value
        End Set
    End Property

    Public Property Id_Tipo_Servicio As Integer
        Get
            Return _idTipoServicio
        End Get
        Set(value As Integer)
            _idTipoServicio = value
        End Set
    End Property

    Public Property Id_Cliente As Integer
        Get
            Return _idCliente
        End Get
        Set(value As Integer)
            _idCliente = value
        End Set
    End Property

    Public Overrides Function obtenerListado() As SqlDataReader
        Me.atributosSelect = "E.*, C.nombre + ' ' + C.apellido AS nombreCliente, C.id As cliente_id"
        Me.joins = " E left join personas C ON C.id = E.id_cliente "
        MyBase.atributosWhere = {}
        Return MyBase.obtenerListado()
        MyBase.atributosWhere = {"Id"}

    End Function



End Class
