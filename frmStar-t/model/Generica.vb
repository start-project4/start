﻿Imports System.Data.SqlClient

Public Class Generica

    ' Public: Todos tienen acceso
    ' Protected: Tengo acceso desde la clase y su herencia, no del resto 
    ' Private: Solamente tengo acceso desde dentro de la clase
    Protected nombreTabla As String
    Protected atributosSelect As String = "*"
    Protected atributosInsert() As String
    Protected atributosWhere() As String
    Protected atributosWhereSelect() As String = {}
    Protected joins As String = ""

    Public Overridable Function obtenerListado() As SqlDataReader
        ' Return DBConn.Instance.SelectStatement("SELECT * FROM " + nombreTabla)
        Dim sqlCmd As New SqlCommand
        Dim selectCommand As String = "SELECT " & atributosSelect & " FROM " & nombreTabla & " " & joins & " "

        Dim listaSet As New List(Of String)

        Dim where As New List(Of String)
        If (atributosWhereSelect.Length > 0) Then
            For Each attrWhere As String In atributosWhere
                Dim pInfo As System.Reflection.PropertyInfo = Me.GetType().GetProperty(attrWhere)

                Dim atributoValue As Object = pInfo.GetValue(Me, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)

                sqlCmd.Parameters.AddWithValue("@" & attrWhere, atributoValue)

                where.Add(attrWhere & "=@" & attrWhere)
            Next
        End If

        If (where.Count > 0) Then
            Dim stringWhere = String.Join(" AND ", where)

            selectCommand += " WHERE " & stringWhere
        End If

        sqlCmd.CommandText = selectCommand
        Return DBConn.Instance.SelectStatement(sqlCmd)
    End Function

    Public Overridable Function Insertar() As Integer
        Dim sqlCmd As New SqlCommand

        sqlCmd.CommandText = "INSERT INTO " & nombreTabla & "(" & String.Join(", ", atributosInsert) &
            ") VALUES (@" & String.Join(", @", atributosInsert) & ")"

        For Each propertyNames As String In atributosInsert
            Dim pInfo As System.Reflection.PropertyInfo = Me.GetType().GetProperty(propertyNames)

            Dim pValue As Object = pInfo.GetValue(Me, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)

            If pValue = Nothing Then
                pValue = DBNull.Value
            End If

            sqlCmd.Parameters.AddWithValue("@" & propertyNames, pValue)

        Next

        Return DBConn.Instance.ABMStatment(sqlCmd)
    End Function

    Public Overridable Function Editar() As Integer
        Dim sqlCmd As New SqlCommand
        Dim updateCommand As String = "UPDATE " & nombreTabla & " SET "

        Dim listaSet As New List(Of String)

        For Each atributo As String In atributosInsert

            Dim pInfo As System.Reflection.PropertyInfo = Me.GetType().GetProperty(atributo)

            Dim atributoValue As Object = pInfo.GetValue(Me, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)

            sqlCmd.Parameters.AddWithValue("@" & atributo, atributoValue)

            listaSet.Add(atributo & "=@" & atributo)

        Next

        updateCommand += String.Join(", ", listaSet)

        Dim where As New List(Of String)

        For Each attrWhere As String In atributosWhere
            Dim pInfo As System.Reflection.PropertyInfo = Me.GetType().GetProperty(attrWhere)

            Dim atributoValue As Object = pInfo.GetValue(Me, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)

            sqlCmd.Parameters.AddWithValue("@" & attrWhere, atributoValue)

            where.Add(attrWhere & "=@" & attrWhere)
        Next

        If (where.Count > 0) Then
            Dim stringWhere = String.Join(" AND ", where)

            updateCommand += " WHERE " & stringWhere
        End If

        sqlCmd.CommandText = updateCommand

        Return DBConn.Instance.ABMStatment(sqlCmd)

    End Function

    Public Overridable Function Borrar(id As String) As Integer
        Dim conn As DBConn = DBConn.Instance()

        Dim consulta As String = "DELETE FROM " & nombreTabla & " WHERE id = " & id

        Dim delete As New SqlCommand(consulta)
        Return DBConn.Instance().ABMStatment(delete)
    End Function

End Class
