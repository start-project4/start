﻿Imports System.Data.SqlClient

Public Class frmTiposServicio
    Private Sub frmTiposServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadData()
    End Sub

    Private Sub LoadData()
        Dim oTipoServicio As New TipoServicio()

        Dim sqlResult As SqlDataReader = oTipoServicio.obtenerListado()

        lvwTiposServicio.Items.Clear()

        If sqlResult.HasRows Then
            While sqlResult.Read
                Dim lvItem As ListViewItem = lvwTiposServicio.Items.Add(sqlResult("id").ToString)
                lvItem.SubItems.Add(sqlResult("nombre").ToString)
                lvItem.SubItems.Add(sqlResult("descripcion").ToString)
                lvItem.SubItems.Add(sqlResult("precio").ToString)

            End While
        End If
        sqlResult.Close()

    End Sub

    Private Sub btnOpenFormAddTipoServicio_Click(sender As Object, e As EventArgs) Handles btnOpenFormAddTipoServicio.Click
        frmAddTipoServicio.Show()
    End Sub

    Private Sub frmTiposServicio_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        LoadData()
    End Sub

    Private Sub lvwTiposServicio_MouseUp(sender As Object, e As MouseEventArgs) Handles lvwTiposServicio.MouseUp
        If e.Button <> MouseButtons.Right Then Return

        ' TODO: Error when I click (right) in ListView that is not in Item
        Dim lvi As ListViewItem = Me.lvwTiposServicio.SelectedItems(0)

        Dim idTipoServicio = lvi.SubItems.Item(0).Text

        Dim nameTipoServicio = lvi.SubItems.Item(1).Text

        Dim cms = New ContextMenuStrip
        Dim itemUpdate = cms.Items.Add("Editar")
        itemUpdate.Tag = idTipoServicio.ToString() + "-" + nameTipoServicio.ToString()
        AddHandler itemUpdate.Click, AddressOf menuChoice
        Dim itemDelete = cms.Items.Add("Eliminar")
        itemDelete.Tag = idTipoServicio.ToString() + "-" + nameTipoServicio.ToString()
        AddHandler itemDelete.Click, AddressOf menuChoice

        cms.Show(lvwTiposServicio, e.Location)
    End Sub

    Private Sub menuChoice(ByVal sender As Object, ByVal e As EventArgs)
        'Se recive el item
        Dim item = CType(sender, ToolStripMenuItem)

        ' Esto contiene el Tag del item
        Dim itemAction = item.Tag

        Dim words As String() = itemAction.Split(New Char() {"-"c})

        Dim idTipoServicio = words(0)
        Dim nameTipoServicio = words(1)

        Dim msgBoxText As String
        Dim msgBoxTitle As String

        Dim resultActionEdit As Integer = -1
        Dim resultActionDelete As Integer = -1

        If item.Text = "Editar" Then
            msgBoxText = "¿Estás seguro que quieres editar el tipo de servicio {tipo_servicio}?"
            msgBoxTitle = "Apagar"
            resultActionEdit = MsgBox(msgBoxText.Replace("{tipo_servicio}", nameTipoServicio), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, msgBoxTitle + " enchufe")
        Else
            msgBoxText = "¿Estás seguro que quieres eliminar el tipo de servicio {tipo_servicio}?"
            msgBoxTitle = "Prender"
            resultActionDelete = MsgBox(msgBoxText.Replace("{tipo_servicio}", nameTipoServicio), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, msgBoxTitle + " enchufe")
        End If


        If (resultActionEdit = 6) Then
            MsgBox("Dijo que si al updatear el tipo de servicio con ID " + idTipoServicio)
        ElseIf (resultActionDelete = 6) Then
            MsgBox("Dijo que si al deletear")
        End If

    End Sub

End Class