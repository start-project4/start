﻿Public Class frmAddTipoServicio
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim name, price As String
        Dim priceInteger As Integer

        name = Trim(txtName.Text)
        price = Trim(txtPrice.Text)

        If name = "" Then
            'Si el usr está vacío, muestro error
            MsgBox("Debes ingresar un nombre", MsgBoxStyle.Exclamation, "Error")
        ElseIf Not Int32.TryParse(price, priceInteger) Then
            'Si el pwd está vacío, muestro error
            MsgBox("Debes ingresar un precio válido", MsgBoxStyle.Exclamation, "Error")
        Else
            'LLamar a función para agregar tipos de servicios

            Dim oTipoServicio As New TipoServicio(name, txtDescription.Text, priceInteger)

            If oTipoServicio.Insertar() = 1 Then
                MsgBox("Se agregó el tipo de servicio " + name, MsgBoxStyle.Exclamation, "Éxito")
            End If
            frmTiposServicio.Close()
            frmTiposServicio.Show()
            Me.Close()
        End If
    End Sub
End Class