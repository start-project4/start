﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTiposServicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwTiposServicio = New System.Windows.Forms.ListView()
        Me.id = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.precio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnOpenFormAddTipoServicio = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lvwTiposServicio
        '
        Me.lvwTiposServicio.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.id, Me.nombre, Me.descripcion, Me.precio})
        Me.lvwTiposServicio.FullRowSelect = True
        Me.lvwTiposServicio.Location = New System.Drawing.Point(13, 37)
        Me.lvwTiposServicio.Name = "lvwTiposServicio"
        Me.lvwTiposServicio.Size = New System.Drawing.Size(775, 280)
        Me.lvwTiposServicio.TabIndex = 0
        Me.lvwTiposServicio.UseCompatibleStateImageBehavior = False
        Me.lvwTiposServicio.View = System.Windows.Forms.View.Details
        '
        'id
        '
        Me.id.Text = "ID"
        '
        'nombre
        '
        Me.nombre.Text = "Nombre"
        '
        'descripcion
        '
        Me.descripcion.Text = "Descripción"
        '
        'precio
        '
        Me.precio.Text = "Precio"
        '
        'btnOpenFormAddTipoServicio
        '
        Me.btnOpenFormAddTipoServicio.Location = New System.Drawing.Point(702, 8)
        Me.btnOpenFormAddTipoServicio.Name = "btnOpenFormAddTipoServicio"
        Me.btnOpenFormAddTipoServicio.Size = New System.Drawing.Size(75, 23)
        Me.btnOpenFormAddTipoServicio.TabIndex = 1
        Me.btnOpenFormAddTipoServicio.Text = "Agregar"
        Me.btnOpenFormAddTipoServicio.UseVisualStyleBackColor = True
        '
        'frmTiposServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnOpenFormAddTipoServicio)
        Me.Controls.Add(Me.lvwTiposServicio)
        Me.Name = "frmTiposServicio"
        Me.Text = "Tipos de servicios"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lvwTiposServicio As ListView
    Friend WithEvents id As ColumnHeader
    Friend WithEvents nombre As ColumnHeader
    Friend WithEvents descripcion As ColumnHeader
    Friend WithEvents precio As ColumnHeader
    Friend WithEvents btnOpenFormAddTipoServicio As Button
End Class
