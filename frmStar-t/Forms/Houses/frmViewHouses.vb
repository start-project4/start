﻿Public Class frmViewHouses
    Private Sub frmViewHouses_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'TODO: Cambiar las fechas de String a Date
        addHouse(1, "Yamila", "Sosa", "F", "yamila.sosa@anima.edu.uy", "01/01/2001")
        addHouse(2, "Pablo", "Maldonado", "M", "pablo.maldonado@anima.edu.uy", "01/01/2001")
        addHouse(3, "Manuel", "Maldonado", "M", "manuel.maldonado@anima.edu.uy", "01/01/2001")
        addHouse(4, "Gonzalo", "Otero", "M", "gonzalo.otero@anima.edu.uy", "01/01/2001")
        addHouse(5, "Alguien", "Mágico", "M", "alguien@alguien.com", "01/01/2001")
        addHouse(5, "Otro", "Alguien", "F", "otro.alguien@cualquiera.uy", "01/01/2001")
        addHouse(6, "Spider", "Man", "M", "Spiderman@supergente.com", "01/01/2001")
        addHouse(7, "Wonder", "Woman", "F", "wonder.woman@supergente.com", "01/01/2001")
        addHouse(8, "Iron", "Man", "M", "iron.man@supergente.com", "01/01/2001")
        addHouse(9, "Paula", "Pereyra", "F", "paula.pereyra@supergente.com", "01/01/2001")
        addHouse(10, "Limón", "Cítrico", "M", "limon@citrico.com", "01/01/2001")

    End Sub

    Private Sub addHouse(id As Integer, name As String, surname As String, gender As String, email As String, birthday As String)
        Dim item As New ListViewItem(id)

        item.SubItems.Add(name)
        item.SubItems.Add(surname)
        item.SubItems.Add(gender)
        item.SubItems.Add(email)
        item.SubItems.Add(birthday)

        lvwHouses.Items.Add(item)
    End Sub

    Private Sub lvwHouses_DoubleClick(sender As Object, e As EventArgs) Handles lvwHouses.DoubleClick
        Dim lvi As ListViewItem = Me.lvwHouses.SelectedItems(0)

        Dim idHouse = lvi.SubItems.Item(0).Text
        Dim nameClient = lvi.SubItems.Item(1).Text
        Dim surnnameClient = lvi.SubItems.Item(2).Text
        Dim genderClient = lvi.SubItems.Item(3).Text
        Dim emailClient = lvi.SubItems.Item(4).Text
        Dim birthdayClient = lvi.SubItems.Item(5).Text

        ' Id is lvi.SubItems.Item(0).Text
        ' And name is lvi.SubItems.Item(1).Text
        'MsgBox("Usted eligió la casa de " + lvi.SubItems.Item(1).Text, MsgBoxStyle.Information, "Casa #" + lvi.SubItems.Item(0).Text)

        Dim frmHouseDetails As New frmviewDetailsHouse()
        frmHouseDetails.Text = "Casa #" + idHouse
        frmHouseDetails.lblHouseId.Text += idHouse
        frmHouseDetails.lblModifyName.Text = nameClient
        frmHouseDetails.lblModifySurname.Text = surnnameClient
        frmHouseDetails.lblModifyGender.Text = genderClient
        frmHouseDetails.lblModifyEmail.Text = emailClient
        frmHouseDetails.lblModifyBirthday.Text = birthdayClient

        frmHouseDetails.Show()

    End Sub
End Class