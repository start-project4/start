﻿Public Class frmViewAdmins

    Private Sub frmViewAdmins_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addAdmin(1, "Admin", "Mayor", "F", "admin.mayor@anima.edu.uy", "02/02/2001", "Mayor")
        addAdmin(2, "Admin", "Menor", "M", "admin.menor@anima.edu.uy", "04/04/2001", "Menor")
    End Sub

    Private Sub addAdmin(id As Integer, name As String, surname As String, gender As String, email As String, birthday As String, typeAdmin As String)
        Dim item As New ListViewItem(id)

        item.SubItems.Add(name)
        item.SubItems.Add(surname)
        item.SubItems.Add(gender)
        item.SubItems.Add(email)
        item.SubItems.Add(birthday)
        item.SubItems.Add(typeAdmin)

        lvwHouses.Items.Add(item)
    End Sub

    Private Sub btnAddAdmin_Click(sender As Object, e As EventArgs) Handles btnAddAdmin.Click
        frmNewAdmin.Show()
    End Sub
End Class