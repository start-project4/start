﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewUser))
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblIdKit = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblSurname = New System.Windows.Forms.Label()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.rbtGenderM = New System.Windows.Forms.RadioButton()
        Me.rbtGenderF = New System.Windows.Forms.RadioButton()
        Me.rbtGenderUndefined = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.txtDateBirthday = New System.Windows.Forms.TextBox()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.gbxPerson = New System.Windows.Forms.GroupBox()
        Me.txtIdKit = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtSurname = New System.Windows.Forms.TextBox()
        Me.gbxCard = New System.Windows.Forms.GroupBox()
        Me.txtCVCCard = New System.Windows.Forms.TextBox()
        Me.lblCVCCard = New System.Windows.Forms.Label()
        Me.txtDateExpiresCard = New System.Windows.Forms.TextBox()
        Me.lblDateExpiresCard = New System.Windows.Forms.Label()
        Me.txtNumberCard = New System.Windows.Forms.TextBox()
        Me.lblNumberCard = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.gbxPerson.SuspendLayout()
        Me.gbxCard.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(519, 476)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(94, 23)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "&Cancelar"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(416, 476)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(94, 23)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Guardar"
        '
        'lblIdKit
        '
        Me.lblIdKit.Location = New System.Drawing.Point(6, 28)
        Me.lblIdKit.Name = "lblIdKit"
        Me.lblIdKit.Size = New System.Drawing.Size(220, 23)
        Me.lblIdKit.TabIndex = 101
        Me.lblIdKit.Text = "ID Kit"
        Me.lblIdKit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(5, 76)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(220, 23)
        Me.lblName.TabIndex = 102
        Me.lblName.Text = "Nombre"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSurname
        '
        Me.lblSurname.Location = New System.Drawing.Point(6, 123)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(220, 23)
        Me.lblSurname.TabIndex = 103
        Me.lblSurname.Text = "Apellido"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGender
        '
        Me.lblGender.Location = New System.Drawing.Point(6, 170)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(220, 23)
        Me.lblGender.TabIndex = 104
        Me.lblGender.Text = "Género"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbtGenderM
        '
        Me.rbtGenderM.AutoSize = True
        Me.rbtGenderM.Location = New System.Drawing.Point(9, 10)
        Me.rbtGenderM.Name = "rbtGenderM"
        Me.rbtGenderM.Size = New System.Drawing.Size(34, 17)
        Me.rbtGenderM.TabIndex = 4
        Me.rbtGenderM.TabStop = True
        Me.rbtGenderM.Text = "M"
        Me.rbtGenderM.UseVisualStyleBackColor = True
        '
        'rbtGenderF
        '
        Me.rbtGenderF.AutoSize = True
        Me.rbtGenderF.Location = New System.Drawing.Point(50, 10)
        Me.rbtGenderF.Name = "rbtGenderF"
        Me.rbtGenderF.Size = New System.Drawing.Size(31, 17)
        Me.rbtGenderF.TabIndex = 5
        Me.rbtGenderF.TabStop = True
        Me.rbtGenderF.Text = "F"
        Me.rbtGenderF.UseVisualStyleBackColor = True
        '
        'rbtGenderUndefined
        '
        Me.rbtGenderUndefined.AutoSize = True
        Me.rbtGenderUndefined.Location = New System.Drawing.Point(88, 10)
        Me.rbtGenderUndefined.Name = "rbtGenderUndefined"
        Me.rbtGenderUndefined.Size = New System.Drawing.Size(94, 17)
        Me.rbtGenderUndefined.TabIndex = 6
        Me.rbtGenderUndefined.TabStop = True
        Me.rbtGenderUndefined.Text = "Sin especificar"
        Me.rbtGenderUndefined.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rbtGenderUndefined)
        Me.Panel1.Controls.Add(Me.rbtGenderM)
        Me.Panel1.Controls.Add(Me.rbtGenderF)
        Me.Panel1.Location = New System.Drawing.Point(4, 194)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(184, 38)
        Me.Panel1.TabIndex = 21
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(6, 264)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtEmail.Size = New System.Drawing.Size(220, 20)
        Me.txtEmail.TabIndex = 7
        '
        'lblEmail
        '
        Me.lblEmail.Location = New System.Drawing.Point(5, 241)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(220, 23)
        Me.lblEmail.TabIndex = 105
        Me.lblEmail.Text = "E-mail"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(6, 314)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPhone.Size = New System.Drawing.Size(220, 20)
        Me.txtPhone.TabIndex = 8
        '
        'lblPhone
        '
        Me.lblPhone.Location = New System.Drawing.Point(6, 291)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(220, 23)
        Me.lblPhone.TabIndex = 106
        Me.lblPhone.Text = "Teléfono celular"
        Me.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDateBirthday
        '
        Me.txtDateBirthday.Location = New System.Drawing.Point(6, 365)
        Me.txtDateBirthday.Name = "txtDateBirthday"
        Me.txtDateBirthday.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDateBirthday.Size = New System.Drawing.Size(220, 20)
        Me.txtDateBirthday.TabIndex = 9
        '
        'lblBirthday
        '
        Me.lblBirthday.Location = New System.Drawing.Point(5, 340)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(220, 23)
        Me.lblBirthday.TabIndex = 107
        Me.lblBirthday.Text = "Fecha de nacimiento"
        Me.lblBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxPerson
        '
        Me.gbxPerson.Controls.Add(Me.txtIdKit)
        Me.gbxPerson.Controls.Add(Me.txtName)
        Me.gbxPerson.Controls.Add(Me.txtSurname)
        Me.gbxPerson.Controls.Add(Me.lblIdKit)
        Me.gbxPerson.Controls.Add(Me.lblName)
        Me.gbxPerson.Controls.Add(Me.txtDateBirthday)
        Me.gbxPerson.Controls.Add(Me.lblBirthday)
        Me.gbxPerson.Controls.Add(Me.lblSurname)
        Me.gbxPerson.Controls.Add(Me.txtPhone)
        Me.gbxPerson.Controls.Add(Me.lblPhone)
        Me.gbxPerson.Controls.Add(Me.lblGender)
        Me.gbxPerson.Controls.Add(Me.txtEmail)
        Me.gbxPerson.Controls.Add(Me.Panel1)
        Me.gbxPerson.Controls.Add(Me.lblEmail)
        Me.gbxPerson.Location = New System.Drawing.Point(46, 21)
        Me.gbxPerson.Name = "gbxPerson"
        Me.gbxPerson.Size = New System.Drawing.Size(284, 410)
        Me.gbxPerson.TabIndex = 100
        Me.gbxPerson.TabStop = False
        Me.gbxPerson.Text = "Datos de la persona"
        '
        'txtIdKit
        '
        Me.txtIdKit.Location = New System.Drawing.Point(6, 50)
        Me.txtIdKit.Name = "txtIdKit"
        Me.txtIdKit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtIdKit.Size = New System.Drawing.Size(220, 20)
        Me.txtIdKit.TabIndex = 1
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(6, 98)
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtName.Size = New System.Drawing.Size(220, 20)
        Me.txtName.TabIndex = 2
        '
        'txtSurname
        '
        Me.txtSurname.Location = New System.Drawing.Point(6, 145)
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSurname.Size = New System.Drawing.Size(220, 20)
        Me.txtSurname.TabIndex = 3
        '
        'gbxCard
        '
        Me.gbxCard.Controls.Add(Me.txtCVCCard)
        Me.gbxCard.Controls.Add(Me.lblCVCCard)
        Me.gbxCard.Controls.Add(Me.txtDateExpiresCard)
        Me.gbxCard.Controls.Add(Me.lblDateExpiresCard)
        Me.gbxCard.Controls.Add(Me.txtNumberCard)
        Me.gbxCard.Controls.Add(Me.lblNumberCard)
        Me.gbxCard.Location = New System.Drawing.Point(351, 21)
        Me.gbxCard.Name = "gbxCard"
        Me.gbxCard.Size = New System.Drawing.Size(225, 146)
        Me.gbxCard.TabIndex = 108
        Me.gbxCard.TabStop = False
        Me.gbxCard.Text = "Datos de la tarjeta"
        '
        'txtCVCCard
        '
        Me.txtCVCCard.Location = New System.Drawing.Point(165, 97)
        Me.txtCVCCard.Name = "txtCVCCard"
        Me.txtCVCCard.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtCVCCard.Size = New System.Drawing.Size(44, 20)
        Me.txtCVCCard.TabIndex = 12
        '
        'lblCVCCard
        '
        Me.lblCVCCard.Location = New System.Drawing.Point(165, 75)
        Me.lblCVCCard.Name = "lblCVCCard"
        Me.lblCVCCard.Size = New System.Drawing.Size(44, 23)
        Me.lblCVCCard.TabIndex = 111
        Me.lblCVCCard.Text = "CVC"
        Me.lblCVCCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDateExpiresCard
        '
        Me.txtDateExpiresCard.Location = New System.Drawing.Point(6, 98)
        Me.txtDateExpiresCard.Name = "txtDateExpiresCard"
        Me.txtDateExpiresCard.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDateExpiresCard.Size = New System.Drawing.Size(137, 20)
        Me.txtDateExpiresCard.TabIndex = 11
        '
        'lblDateExpiresCard
        '
        Me.lblDateExpiresCard.Location = New System.Drawing.Point(6, 76)
        Me.lblDateExpiresCard.Name = "lblDateExpiresCard"
        Me.lblDateExpiresCard.Size = New System.Drawing.Size(153, 23)
        Me.lblDateExpiresCard.TabIndex = 110
        Me.lblDateExpiresCard.Text = "Fecha de vencimiento"
        Me.lblDateExpiresCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNumberCard
        '
        Me.txtNumberCard.Location = New System.Drawing.Point(6, 50)
        Me.txtNumberCard.Name = "txtNumberCard"
        Me.txtNumberCard.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNumberCard.Size = New System.Drawing.Size(203, 20)
        Me.txtNumberCard.TabIndex = 10
        '
        'lblNumberCard
        '
        Me.lblNumberCard.Location = New System.Drawing.Point(6, 28)
        Me.lblNumberCard.Name = "lblNumberCard"
        Me.lblNumberCard.Size = New System.Drawing.Size(220, 23)
        Me.lblNumberCard.TabIndex = 109
        Me.lblNumberCard.Text = "Número de tarjeta"
        Me.lblNumberCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmNewUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(668, 527)
        Me.Controls.Add(Me.gbxCard)
        Me.Controls.Add(Me.gbxPerson)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmNewUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agregar Usuario"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbxPerson.ResumeLayout(False)
        Me.gbxPerson.PerformLayout()
        Me.gbxCard.ResumeLayout(False)
        Me.gbxCard.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnCancel As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents lblIdKit As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblSurname As Label
    Friend WithEvents lblGender As Label
    Friend WithEvents rbtGenderM As RadioButton
    Friend WithEvents rbtGenderF As RadioButton
    Friend WithEvents rbtGenderUndefined As RadioButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents lblEmail As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents lblPhone As Label
    Friend WithEvents txtDateBirthday As TextBox
    Friend WithEvents lblBirthday As Label
    Friend WithEvents gbxPerson As GroupBox
    Friend WithEvents txtIdKit As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtSurname As TextBox
    Friend WithEvents gbxCard As GroupBox
    Friend WithEvents txtDateExpiresCard As TextBox
    Friend WithEvents lblDateExpiresCard As Label
    Friend WithEvents txtNumberCard As TextBox
    Friend WithEvents lblNumberCard As Label
    Friend WithEvents txtCVCCard As TextBox
    Friend WithEvents lblCVCCard As Label
End Class
