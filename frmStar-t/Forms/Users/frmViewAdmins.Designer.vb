﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewAdmins
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwHouses = New System.Windows.Forms.ListView()
        Me.idHouse = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.nameUser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.surnameUser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.genderUser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.emailUser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.birthdayUser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.typeAdmin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAddAdmin = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lvwHouses
        '
        Me.lvwHouses.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idHouse, Me.nameUser, Me.surnameUser, Me.genderUser, Me.emailUser, Me.birthdayUser, Me.typeAdmin})
        Me.lvwHouses.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvwHouses.FullRowSelect = True
        Me.lvwHouses.Location = New System.Drawing.Point(12, 39)
        Me.lvwHouses.Name = "lvwHouses"
        Me.lvwHouses.Size = New System.Drawing.Size(1229, 458)
        Me.lvwHouses.TabIndex = 7
        Me.lvwHouses.UseCompatibleStateImageBehavior = False
        Me.lvwHouses.View = System.Windows.Forms.View.Details
        '
        'idHouse
        '
        Me.idHouse.Text = "ID"
        Me.idHouse.Width = 81
        '
        'nameUser
        '
        Me.nameUser.Text = "Nombre"
        Me.nameUser.Width = 217
        '
        'surnameUser
        '
        Me.surnameUser.Text = "Apellido"
        Me.surnameUser.Width = 248
        '
        'genderUser
        '
        Me.genderUser.Text = "Género"
        '
        'emailUser
        '
        Me.emailUser.Text = "E-Mail"
        Me.emailUser.Width = 250
        '
        'birthdayUser
        '
        Me.birthdayUser.Text = "Fecha de nacimiento"
        Me.birthdayUser.Width = 160
        '
        'typeAdmin
        '
        Me.typeAdmin.Text = "Tipo de Administrador"
        Me.typeAdmin.Width = 180
        '
        'btnAddAdmin
        '
        Me.btnAddAdmin.Location = New System.Drawing.Point(1166, 9)
        Me.btnAddAdmin.Name = "btnAddAdmin"
        Me.btnAddAdmin.Size = New System.Drawing.Size(75, 23)
        Me.btnAddAdmin.TabIndex = 8
        Me.btnAddAdmin.Text = "Añadir"
        Me.btnAddAdmin.UseVisualStyleBackColor = True
        '
        'frmViewAdmins
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1261, 519)
        Me.Controls.Add(Me.btnAddAdmin)
        Me.Controls.Add(Me.lvwHouses)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmViewAdmins"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Administradores"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lvwHouses As ListView
    Friend WithEvents idHouse As ColumnHeader
    Friend WithEvents nameUser As ColumnHeader
    Friend WithEvents surnameUser As ColumnHeader
    Friend WithEvents genderUser As ColumnHeader
    Friend WithEvents emailUser As ColumnHeader
    Friend WithEvents birthdayUser As ColumnHeader
    Friend WithEvents typeAdmin As ColumnHeader
    Friend WithEvents btnAddAdmin As Button
End Class
