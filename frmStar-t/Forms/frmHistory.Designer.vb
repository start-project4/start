﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHistory
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHistory))
        Me.lvwHistory = New System.Windows.Forms.ListView()
        Me.idHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.accionHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idHouseHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idEnchufeHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.integranteHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.dateHistory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'lvwHistory
        '
        Me.lvwHistory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idHistory, Me.accionHistory, Me.idHouseHistory, Me.idEnchufeHistory, Me.integranteHistory, Me.dateHistory})
        Me.lvwHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvwHistory.FullRowSelect = True
        Me.lvwHistory.Location = New System.Drawing.Point(8, 8)
        Me.lvwHistory.Name = "lvwHistory"
        Me.lvwHistory.Size = New System.Drawing.Size(1070, 397)
        Me.lvwHistory.TabIndex = 7
        Me.lvwHistory.UseCompatibleStateImageBehavior = False
        Me.lvwHistory.View = System.Windows.Forms.View.Details
        '
        'idHistory
        '
        Me.idHistory.Text = "ID"
        Me.idHistory.Width = 81
        '
        'accionHistory
        '
        Me.accionHistory.Text = "Acción"
        Me.accionHistory.Width = 217
        '
        'idHouseHistory
        '
        Me.idHouseHistory.Text = "ID de la casa"
        Me.idHouseHistory.Width = 143
        '
        'idEnchufeHistory
        '
        Me.idEnchufeHistory.Text = "ID del enchufe"
        Me.idEnchufeHistory.Width = 145
        '
        'integranteHistory
        '
        Me.integranteHistory.Text = "Quién hizo"
        Me.integranteHistory.Width = 248
        '
        'dateHistory
        '
        Me.dateHistory.Text = "Fecha"
        Me.dateHistory.Width = 232
        '
        'frmHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1087, 425)
        Me.Controls.Add(Me.lvwHistory)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmHistory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Historial"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lvwHistory As ListView
    Friend WithEvents idHistory As ColumnHeader
    Friend WithEvents accionHistory As ColumnHeader
    Friend WithEvents integranteHistory As ColumnHeader
    Friend WithEvents dateHistory As ColumnHeader
    Friend WithEvents idHouseHistory As ColumnHeader
    Friend WithEvents idEnchufeHistory As ColumnHeader
End Class
