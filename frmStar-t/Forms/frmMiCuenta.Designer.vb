﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMiCuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMiCuenta))
        Me.pcxImagePerson = New System.Windows.Forms.PictureBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblGradoAdministrador = New System.Windows.Forms.Label()
        Me.lblModifyGrado = New System.Windows.Forms.Label()
        Me.btnChangePassword = New System.Windows.Forms.Button()
        Me.lblSurnamePerson = New System.Windows.Forms.Label()
        Me.lblEmailPerson = New System.Windows.Forms.Label()
        Me.lblModifyName = New System.Windows.Forms.Label()
        Me.lblModifySurname = New System.Windows.Forms.Label()
        Me.lblModifyEmail = New System.Windows.Forms.Label()
        CType(Me.pcxImagePerson, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcxImagePerson
        '
        Me.pcxImagePerson.Image = CType(resources.GetObject("pcxImagePerson.Image"), System.Drawing.Image)
        Me.pcxImagePerson.Location = New System.Drawing.Point(36, 25)
        Me.pcxImagePerson.Name = "pcxImagePerson"
        Me.pcxImagePerson.Size = New System.Drawing.Size(255, 229)
        Me.pcxImagePerson.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pcxImagePerson.TabIndex = 1
        Me.pcxImagePerson.TabStop = False
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(310, 68)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(69, 20)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Nombre:"
        '
        'lblGradoAdministrador
        '
        Me.lblGradoAdministrador.AutoSize = True
        Me.lblGradoAdministrador.Location = New System.Drawing.Point(44, 269)
        Me.lblGradoAdministrador.Name = "lblGradoAdministrador"
        Me.lblGradoAdministrador.Size = New System.Drawing.Size(119, 13)
        Me.lblGradoAdministrador.TabIndex = 5
        Me.lblGradoAdministrador.Text = "Grado de administrador:"
        '
        'lblModifyGrado
        '
        Me.lblModifyGrado.AutoSize = True
        Me.lblModifyGrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyGrado.Location = New System.Drawing.Point(162, 268)
        Me.lblModifyGrado.Name = "lblModifyGrado"
        Me.lblModifyGrado.Size = New System.Drawing.Size(105, 16)
        Me.lblModifyGrado.TabIndex = 6
        Me.lblModifyGrado.Text = "Hermano menor"
        '
        'btnChangePassword
        '
        Me.btnChangePassword.Location = New System.Drawing.Point(545, 12)
        Me.btnChangePassword.Name = "btnChangePassword"
        Me.btnChangePassword.Size = New System.Drawing.Size(111, 23)
        Me.btnChangePassword.TabIndex = 7
        Me.btnChangePassword.Text = "Cambiar contraseña"
        Me.btnChangePassword.UseVisualStyleBackColor = True
        '
        'lblSurnamePerson
        '
        Me.lblSurnamePerson.AutoSize = True
        Me.lblSurnamePerson.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurnamePerson.Location = New System.Drawing.Point(310, 126)
        Me.lblSurnamePerson.Name = "lblSurnamePerson"
        Me.lblSurnamePerson.Size = New System.Drawing.Size(69, 20)
        Me.lblSurnamePerson.TabIndex = 8
        Me.lblSurnamePerson.Text = "Apellido:"
        '
        'lblEmailPerson
        '
        Me.lblEmailPerson.AutoSize = True
        Me.lblEmailPerson.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmailPerson.Location = New System.Drawing.Point(310, 182)
        Me.lblEmailPerson.Name = "lblEmailPerson"
        Me.lblEmailPerson.Size = New System.Drawing.Size(57, 20)
        Me.lblEmailPerson.TabIndex = 9
        Me.lblEmailPerson.Text = "E-mail:"
        '
        'lblModifyName
        '
        Me.lblModifyName.AutoSize = True
        Me.lblModifyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyName.Location = New System.Drawing.Point(386, 67)
        Me.lblModifyName.Name = "lblModifyName"
        Me.lblModifyName.Size = New System.Drawing.Size(62, 24)
        Me.lblModifyName.TabIndex = 10
        Me.lblModifyName.Text = "Public"
        '
        'lblModifySurname
        '
        Me.lblModifySurname.AutoSize = True
        Me.lblModifySurname.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifySurname.Location = New System.Drawing.Point(385, 124)
        Me.lblModifySurname.Name = "lblModifySurname"
        Me.lblModifySurname.Size = New System.Drawing.Size(105, 24)
        Me.lblModifySurname.TabIndex = 11
        Me.lblModifySurname.Text = "Maldonado"
        '
        'lblModifyEmail
        '
        Me.lblModifyEmail.AutoSize = True
        Me.lblModifyEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModifyEmail.Location = New System.Drawing.Point(386, 182)
        Me.lblModifyEmail.Name = "lblModifyEmail"
        Me.lblModifyEmail.Size = New System.Drawing.Size(252, 20)
        Me.lblModifyEmail.TabIndex = 12
        Me.lblModifyEmail.Text = "public.maldonado@anima.edu.uy"
        '
        'frmMiCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(676, 313)
        Me.Controls.Add(Me.lblModifyEmail)
        Me.Controls.Add(Me.lblModifySurname)
        Me.Controls.Add(Me.lblModifyName)
        Me.Controls.Add(Me.lblEmailPerson)
        Me.Controls.Add(Me.lblSurnamePerson)
        Me.Controls.Add(Me.btnChangePassword)
        Me.Controls.Add(Me.lblModifyGrado)
        Me.Controls.Add(Me.lblGradoAdministrador)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.pcxImagePerson)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmMiCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mi Cuenta"
        Me.TopMost = True
        CType(Me.pcxImagePerson, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pcxImagePerson As PictureBox
    Friend WithEvents lblName As Label
    Friend WithEvents lblGradoAdministrador As Label
    Friend WithEvents lblModifyGrado As Label
    Friend WithEvents btnChangePassword As Button
    Friend WithEvents lblSurnamePerson As Label
    Friend WithEvents lblEmailPerson As Label
    Friend WithEvents lblModifyName As Label
    Friend WithEvents lblModifySurname As Label
    Friend WithEvents lblModifyEmail As Label
End Class
