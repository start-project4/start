﻿Public Class frmHistory
    Private Sub frmHistory_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addRegistro(1, "Prender", 4, 2, "Admin2", "22/07/2018 18:40")
        addRegistro(2, "Apagar", 2, 4, "Admin4", "24/8/2018 08:20")
    End Sub

    Private Sub addRegistro(idHistory As Integer, accionHistory As String, idHouseHistory As Integer, idEnchufeHistory As Integer, integranteHistory As String, dateHistory As String)
        Dim item As New ListViewItem(idHistory)

        item.SubItems.Add(accionHistory)
        item.SubItems.Add(idHouseHistory)
        item.SubItems.Add(idEnchufeHistory)
        item.SubItems.Add(integranteHistory)
        item.SubItems.Add(dateHistory)

        lvwHistory.Items.Add(item)
    End Sub
End Class